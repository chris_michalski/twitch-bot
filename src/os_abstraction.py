"""
This file is intended to be used to abstract operating system specific
functions from the bot.
"""
import platform

# Constants that determine what OS the user is on.
WIN_PLATFORM = "WINDOWS"
MAC_PLATFORM = "MAC"
LINUX_PLATFORM = "LINUX"
CUR_PLATFORM = None

WINDOWS_INFORMATION = None
MAC_INFORMATION = None
LINUX_INFORMATION = None

try:
    WINDOWS_INFORMATION = platform.win32_ver()
except Exception as exception:
    print("Exception thrown trying to get Windows information.")
    print(exception)
try:
    MAC_INFORMATION = platform.mac_ver()
except Exception as exception:
    print("Exception thrown trying to get Mac information.")
    print(exception)
try:
    LINUX_INFORMATION = platform.linux_distribution()
except Exception as exception:
    print("Exception thrown trying to get Linux information.")
    print(exception)
# The platform you are on should return a non-empty tuple.
if WINDOWS_INFORMATION is not None and len(WINDOWS_INFORMATION[0]) > 0:
    CUR_PLATFORM = WIN_PLATFORM
elif MAC_INFORMATION is not None and len(MAC_INFORMATION[0]) > 0:
    CUR_PLATFORM = MAC_PLATFORM
elif LINUX_INFORMATION is not None and len(LINUX_INFORMATION[0]) > 0:
    CUR_PLATFORM = LINUX_PLATFORM


def get_filename(filename):
    """
    Gets the correct name of the file depending on the platform.
    """
    prefix = None

    if CUR_PLATFORM == WIN_PLATFORM:
        prefix = "_"
    else:
        prefix = "."

    os_filename = prefix + filename
    return os_filename
