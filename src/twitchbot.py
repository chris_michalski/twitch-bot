"""
This bot is intended to be used with Twitch.tv IRC chat to help moderate as
well as serve other functions. These other functions can include but are not
limited to chat games, chat quotes, chat logs, etc. The general idea is to make
the life of the broadcaster easier.

To run this bot run the following in a command window:
python twitchbot.py
or if you have multiple versions of Python installed:
python3 twitchbot.py
"""
# System imports
import collections
import datetime
import errno
import getpass
import json
import logging
import os
import random
import signal
import socket
import sys
import textwrap
import time
import traceback
try:
    import urllib.request
except ImportError as import_error:
    print(import_error)
# Local imports
import os_abstraction as osa


# Shorthand constants for logging messages.
DEBUG = logging.DEBUG
INFO = logging.INFO
WARN = logging.WARN
ERROR = logging.ERROR

# Formats for different kinds of messages.
CHAT_MESSAGE_FORMAT = "PRIVMSG %s :%s"
WHISPER_MESSAGE_FORMAT = "PRIVMSG %s :/w %s"

# Different badges that a user can have.
BADGE_TWITCH_STAFF = "Ts"
BADGE_ADMIN = "A"
BADGE_GLOBAL_MOD = "G"
BADGE_BROADCASTER = "B"
BADGE_MOD = "M"
BADGE_TURBO = "T"
BADGE_SUBSCRIBER = "S"
BADGE_TWITCH_PRIME = "P"

class TwitchBot(object):
    """
    The main class of the Twitch bot. The brains of bot.
    """
    HOST = "irc.chat.twitch.tv"
    PORT = 6667
    OAUTH_FILENAME = "oauth"
    SETTINGS_FILENAME = "config/bot_settings.json"
    BUILT_IN_COMMANDS_FILENAME = "config/built_in_commands.json"
    CUSTOM_COMMANDS_FILENAME = "config/custom_commands.json"
    CHAT_PACKS_FILENAME = "config/chat_packs.json"
    QUOTES_FILENAME = "config/quotes.json"
    TIMED_COMMANDS_FILENAME = "config/timed_commands.json"
    POINTS_FILENAME = "config/points.json"
    HIDDEN_TEXT_STRING = "<HIDDEN>"

    def __init__(self):
        """
        Initializes the bot.
        """
        twitchbot_filepath = os.path.dirname(os.path.realpath(__file__))
        self.root_directory = os.path.abspath(os.path.join(twitchbot_filepath, os.pardir))

        bot_start_time = datetime.datetime.now()
        log_time = bot_start_time.strftime("%Y.%m.%d-%H.%M.%S.%f-")
        log_filename = log_time + "bot.log"
        log_file_path = self.__get_log_file_path(log_filename)
        logging.basicConfig(filename=log_file_path, level=logging.DEBUG)
        self.wrapper = textwrap.TextWrapper(width=80, subsequent_indent=" "*9)

        self.bot_start_time = bot_start_time

        self.readbuffer = ""

        self.settings = self._get_settings()
        if self.settings is None:
            self.printlog("self.settings is set to None. Errors may occur.", WARN)
            self.settings = {}

        self.printlog("Current running process: " + str(os.getpid()), DEBUG)

        self.nick = self.__get_nick()
        self.display_name = None

        self.chat = self.__get_chat()

        self.allow_bang_commands = False

        self.keep_running = True

        self.console_text_wrapper = None
        self.server_connection = None

        signal.signal(signal.SIGINT, self.__handle_sigint)
        return

    def __is_mod_in_chat(self, oauth_pass):
        """
        Checks if the user/bot is a mod in the channel.
        """
        url_request = "https://tmi.twitch.tv/group/user/%s/chatters" % (self.settings["_chat"])
        oauth_header = "Authorization"
        oauth_argument = "OAuth %s" % (oauth_pass)
        accept_header = "Accept"
        accept_argument = "application/vnd.twitchtv.v3+json"

        is_mod = False
        retries = 0
        self.printlog("Checking if %s is a mod in %s's room." % (self.display_name, self.chat), DEBUG)
        # TODO(Chris): Fix this...
        # 17:01:47 Checking if SpappyBot is a mod in #modify12's room.
        # Traceback (most recent call last):
        #   File "twitchbot.py", line 1238, in <module>
        #     BOT.connect()
        #   File "twitchbot.py", line 444, in connect
        #     self.allow_bang_commands = self.__is_mod_in_chat(oauth_pass.strip())
        #   File "twitchbot.py", line 146, in __is_mod_in_chat
        #     r = urllib.request.urlopen(req)
        #   File "/usr/lib64/python3.4/urllib/request.py", line 161, in urlopen
        #     return opener.open(url, data, timeout)
        #   File "/usr/lib64/python3.4/urllib/request.py", line 470, in open
        #     response = meth(req, response)
        #   File "/usr/lib64/python3.4/urllib/request.py", line 580, in http_response
        #     'http', request, response, code, msg, hdrs)
        #   File "/usr/lib64/python3.4/urllib/request.py", line 508, in error
        #     return self._call_chain(*args)
        #   File "/usr/lib64/python3.4/urllib/request.py", line 442, in _call_chain
        #     result = func(*args)
        #   File "/usr/lib64/python3.4/urllib/request.py", line 588, in http_error_default
        #     raise HTTPError(req.full_url, code, msg, hdrs, fp)
        # urllib.error.HTTPError: HTTP Error 503: Service Unavailable
        while self.keep_running and retries < 30 and not is_mod:
            try:
                req = urllib.request.Request(url_request)
                req.add_header(oauth_header, oauth_argument)
                req.add_header(accept_header, accept_argument)
                r = urllib.request.urlopen(req)
                data = json.loads(r.read().decode("utf-8"))
                if self.settings["_username"] in data["chatters"]["moderators"]:
                    is_mod = True
                else:
                    retries = retries + 1
                    time.sleep(1)
                if retries == 9:
                    self.printlog("Waiting 20 more seconds to determine if %s is a mod." % (self.display_name), DEBUG)
                elif retries == 19:
                    self.printlog("Waiting 10 more seconds to determine if %s is a mod." % (self.display_name), DEBUG)
            except Exception as exception:
                self.printlog("Error trying to determine if %s is a mod." % (self.display_name), WARN)
                self.printlog(str(exception), WARN)
        if is_mod:
            self.printlog("%s is a mod in %s's room." % (self.display_name, self.chat))
        else:
            self.printlog("%s is not a mod in %s's room." % (self.display_name, self.chat))
        return is_mod

    def __get_log_file_path(self, log_name):
        """
        Returns the full path of the log file.
        """
        path_to_logs = os.path.join(self.root_directory, "logs")
        if not os.path.exists(path_to_logs):
            os.mkdir(path_to_logs)
        log_file_path = os.path.join(self.root_directory, "logs", log_name)
        return log_file_path

    def __get_file_path(self, filename):
        """
        Simply appends the filename to the directory where the bot program is
        stored.
        """
        file_path = os.path.join(self.root_directory, filename)
        return file_path

    def __handle_sigint(self, my_signal, frame):
        """
        Handler for the interrupt signal.
        Used for when the user presses CTRL+C.
        """
        self.printlog("Killing bot.")
        self.keep_running = False
        self.printlog("Signal value of: %d" % (my_signal), DEBUG)
        return

    def __remove_file(self, filename):
        """
        Removes a file from the current directory.
        """
        success = False
        file_path = self.__get_file_path(filename)

        if os.path.isfile(file_path):
            try:
                self.printlog("Attempting to remove file: %s" % (file_path), DEBUG)
                os.remove(file_path)
                self.printlog("Successfully removed file: %s" % (file_path), DEBUG)
                success = True
            except Exception as exception:
                self.printlog("Error trying to remove existing file: %s" % (file_path), WARN)
                self.printlog(str(exception), WARN)
        else:
            self.printlog("File %s does not exist." % (file_path), DEBUG)

        return success

    def __get_chat(self):
        """
        Gets the name of the chat to connect to.
        """
        self.printlog("Attempting to get the name of the chat.", DEBUG)
        chat_to_use = self.settings["_chat"]
        if chat_to_use[0] != "#":
            chat_to_use = "#" + chat_to_use
        self.printlog("Using the chat: %s" % (chat_to_use), DEBUG)

        return chat_to_use

    def __get_nick(self):
        """
        Gets the nickname (or user name), of the bot, from the user.
        """
        self.printlog("Attempting to get the username.", DEBUG)
        nick_to_use = self.settings["_username"]
        if len(nick_to_use) == 0:
            self.printlog("Username not found in settings file, asking user...", DEBUG)
            # prompt the user for their information
            username = ""
            while len(username) == 0:
                username = input("Account to use: ")
            nick_to_use = username.lower()
            self.printlog("Using username: %s" % (nick_to_use), DEBUG)
        else:
            self.printlog("Username (%s) found in settings file." % (nick_to_use), DEBUG)

        return nick_to_use

    def __get_oauth_from_user(self, oauth_filename):
        """
        Prompts the user for their oauth password.
        """
        oauth_pass = ""
        while len(oauth_pass) == 0:
            oauth_pass = getpass.getpass(prompt="oauth: ", stream=None)
        try:
            oauth_path = self.__get_file_path(oauth_filename)
            oauth_file = open(oauth_path, 'a')
            oauth_name_pass = "%s:%s\n" % (self.nick, oauth_pass)
            try:
                oauth_file.write(oauth_name_pass)
            except Exception as exception:
                self.printlog("Error trying to write to file: %s" % (oauth_path), WARN)
                self.printlog(str(exception), WARN)
            finally:
                oauth_file.close()
        except Exception as exception:
            self.printlog("Error trying to open file: %s" % (oauth_path), WARN)
            self.printlog(str(exception), WARN)

        return oauth_pass

    def __get_oauth_from_file(self, oauth_filename):
        """
        Reads the oauth file and tries to extract the oauth password based off
        of the user name being used.
        """
        oauth_to_use = None
        try:
            oauth_path = self.__get_file_path(oauth_filename)
            oauth_file = open(oauth_path, 'r')
            try:
                oauth_file_lines = oauth_file.readlines()
                oauth_to_use = None
                for line in oauth_file_lines:
                    username, oauth = line.split(":")
                    if self.nick == username:
                        oauth_to_use = oauth
                        break
            except Exception as exception:
                self.printlog("Error trying to read from file: %s" % (oauth_path), WARN)
                self.printlog(str(exception), WARN)
            finally:
                oauth_file.close()
        except Exception as exception:
            self.printlog("Error trying to open file: %s" % (oauth_path), WARN)
            self.printlog(str(exception), WARN)

        return oauth_to_use

    def __get_oauth(self):
        """
        Gets the oauth of the bot from the user.
        """
        oauth_to_use = None
        oauth_filename = osa.get_filename(TwitchBot.OAUTH_FILENAME)
        oauth_path = self.__get_file_path(oauth_filename)

        if not os.path.isfile(oauth_path):
            oauth_to_use = self.__get_oauth_from_user(oauth_path)
        else:
            oauth_to_use = self.__get_oauth_from_file(oauth_path)
            if oauth_to_use is None:
                oauth_to_use = self.__get_oauth_from_user(oauth_path)

        if oauth_to_use is None:
            self.printlog("Something went wrong trying to get the oauth password.", WARN)

        return oauth_to_use

    @staticmethod
    def __hide_text(message, hide_text):
        if hide_text is not None:
            if not isinstance(hide_text, list):
                hide_text = [hide_text]
            for text in hide_text:
                message = message.replace(text.strip(), TwitchBot.HIDDEN_TEXT_STRING)
        return message

    def __send_message(self, message, hide_text=None):
        """
        Sends a message to the chat the bot is connected to.
        """
        if len(message) <= 0:
            self.printlog("Trying to send an empty message.", DEBUG)
            return

        self.settings["current_messages"] += 1
        self.settings["current_messages_sent"] += 1

        message = message.rstrip()
        message = message + "\r\n"
        to_send = bytes(message, "UTF-8")
        # Make sure the message is sent correctly, but the print/log is hidden
        self.server_connection.send(to_send)
        message = self.__hide_text(message, hide_text)
        message = message.strip()
        self.printlog("Message sent: %s" % (message), DEBUG)
        return

    def send_chat_message(self, message):
        """
        Sends a message to the chat the bot is connected to.
        """
        if len(message) <= 0:
            self.printlog("Trying to send an empty chat message.", DEBUG)
            return

        to_send = CHAT_MESSAGE_FORMAT % (self.chat, message)
        self.__send_message(to_send)
        name = self.display_name
        if name is None:
            name = self.nick
        self.printlog("%s: %s" % (name, message))
        return

    def send_whisper_message(self, message):
        """
        Sends a whisper message.
        """
        if len(message) <= 0:
            self.printlog("Trying to send an empty private message.", DEBUG)
            return

        message = WHISPER_MESSAGE_FORMAT % (self.chat, message)
        self.__send_message(message)
        return

    def connect(self):
        """
        Connects the bot to the chat.
        """
        oauth_pass = self.__get_oauth()
        password = "oauth:%s" % (oauth_pass)

        self.server_connection = socket.socket()
        self.server_connection.connect((TwitchBot.HOST, TwitchBot.PORT))
        self.__send_message("PASS %s" % (password), hide_text=oauth_pass)
        self.__send_message("NICK %s" % (self.nick))
        self.__send_message("JOIN %s" % (self.chat))

        self.__send_message("CAP REQ :twitch.tv/membership")
        self.__send_message("CAP REQ :twitch.tv/tags")

        self.server_connection.setblocking(0)

        # TODO(Chris): Find out if this is successful, then do things.
        #
        # Not successful:
        # 22:09:21 :tmi.twitch.tv NOTICE * :Login unsuccessful
        #
        # Successful:
        # 22:11:25 :tmi.twitch.tv 001 modify12 :Welcome, GLHF!
        # 22:11:25 :tmi.twitch.tv 002 modify12 :Your host is tmi.twitch.tv
        # 22:11:25 :tmi.twitch.tv 003 modify12 :This server is rather new
        # 22:11:25 :tmi.twitch.tv 004 modify12 :-
        # 22:11:25 :tmi.twitch.tv 375 modify12 :-
        # 22:11:25 :tmi.twitch.tv 372 modify12 :You are in a maze of twisty passages,
        #          all alike.
        # 22:11:25 :tmi.twitch.tv 376 modify12 :>
        # 22:11:25 :modify12!modify12@modify12.tmi.twitch.tv JOIN #modify12
        # 22:11:25 modify12 joined the channed.
        # 22:11:25 :modify12.tmi.twitch.tv 353 modify12 = #modify12 :modify12
        # 22:11:25 :modify12.tmi.twitch.tv 366 modify12 #modify12 :End of /NAMES
        #          list
        # 22:11:25 :tmi.twitch.tv CAP * ACK :twitch.tv/membership
        # 22:11:25 :tmi.twitch.tv CAP * ACK :twitch.tv/tags
        # 22:11:28 @badges=broadcaster/1;color=#8A2BE2;display-
        #          name=Modify12;emotes=;id=4d9c915f-2999-4143-ac95-9d7d468f25b5;mod=0
        #          ;room-id=32072004;subscriber=0;turbo=0;user-id=32072004;user-type=
        #          :modify12!modify12@modify12.tmi.twitch.tv PRIVMSG #modify12 :a

        url_string = "https://api.twitch.tv/kraken/channels/%s?oauth_token=%s" % (self.nick, oauth_pass)
        api_request = urllib.request.Request(url_string)
        api_response = urllib.request.urlopen(api_request)
        if api_response.getcode() == 200:
            data = json.loads(api_response.read().decode("utf-8"))
            self.display_name = data["display_name"]
            self.printlog("Display name: %s" % (self.display_name), DEBUG)
        else:
            self.printlog("Unable to get the display name of the bot.", DEBUG)

        self.allow_bang_commands = self.__is_mod_in_chat(oauth_pass.strip())

        return

    @staticmethod
    def get_info_and_message(line):
        """
        Organizes a response received from the server.
        """
        info = {}

        parts = line[1:].split(" ", 4)
        user_info = parts[0].split(";")
        info["connection_info"] = parts[1][1:]
        info["message_type"] = parts[2]
        info["channel"] = parts[3]
        message = parts[4][1:]

        info["badges"] = user_info[0].split("=")[1].split(",")
        info["color"] = user_info[1].split("=")[1]
        info["display_name"] = user_info[2].split("=")[1]
        info["emotes"] = user_info[3].split("=")[1]
        info["user_id"] = user_info[4].split("=")[1]
        info["mod_status"] = user_info[5].split("=")[1]
        info["room_id"] = user_info[6].split("=")[1]
        info["subscriber_status"] = user_info[7].split("=")[1]
        info["turbo_status"] = user_info[8].split("=")[1]
        info["user_id"] = user_info[9].split("=")[1]
        info["user_type"] = user_info[10].split("=")[1]

        return info, message

    @staticmethod
    def is_twitch_staff(info):
        """
        Checks to see if the user is Twitch staff.
        """
        twitch_staff = False
        return twitch_staff

    @staticmethod
    def is_admin(info):
        """
        Checks if the user is a Twitch administrator.
        """
        admin = False
        return admin

    @staticmethod
    def is_global_mod(info):
        """
        Checks if the user is a global moderator.
        """
        global_mod = False
        return global_mod

    @staticmethod
    def is_broadcaster(info):
        """
        Checks if the user is the broadcaster of the channel.
        """
        broadcaster = False
        if "broadcaster/1" in info["badges"]:
            broadcaster = True
        return broadcaster

    @staticmethod
    def is_mod(info):
        """
        Checks if the user is a moderator of the channel.
        """
        mod = False
        if info["mod_status"] == "1":
            mod = True
        if info["user_type"] == "mod":
            mod = True
        return mod

    @staticmethod
    def is_turbo(info):
        """
        Checks if the user is a Twitch Turbo user.
        """
        turbo = False
        if "turbo/1" in info["badges"]:
            turbo = True
        return turbo

    @staticmethod
    def is_subscriber(info):
        """
        Checks if the user is a subscriber of the channel.
        """
        subscriber = False
        if "subscriber/1" in info["badges"]:
            subscriber = True
        return subscriber

    @staticmethod
    def is_twitch_prime(info):
        """
        Checks if the user is a Twitch Prime member.
        """
        twitch_prime = False
        if "premium/1" in info["badges"]:
            twitch_prime = True
        return twitch_prime

    def get_badges(self, info):
        """
        Gets the badges of the user.
        """
        badges = ""
        if self.is_twitch_staff(info):
            badges += BADGE_TWITCH_STAFF
        if self.is_admin(info):
            badges += BADGE_ADMIN
        if self.is_global_mod(info):
            badges += BADGE_GLOBAL_MOD
        if self.is_broadcaster(info):
            badges += BADGE_BROADCASTER
        if self.is_mod(info):
            badges += BADGE_MOD
        if self.is_turbo(info):
            badges += BADGE_TURBO
        if self.is_subscriber(info):
            badges += BADGE_SUBSCRIBER
        if self.is_twitch_prime(info):
            badges += BADGE_TWITCH_PRIME
        if len(badges) > 0:
            badges = badges + " "
        return badges

    def __handle_help_command(self, command, arguments, username):
        """
        Handles the built-in command 'help'.
        If there is information on how to use the built-in command, the
        information is displayed in chat.
        """
        if len(arguments) != 1:
            self.send_chat_message(self.settings["built_in_commands"][command]["help"])
            return

        # Attempt to find the built-in command and display the help.
        for built_in_command in self.settings["built_in_commands"]:
            if arguments[0] in self.settings["built_in_commands"][built_in_command]["commands"]:
                self.send_chat_message(self.settings["built_in_commands"][built_in_command]["help"])
                break
        return

    def __handle_enable_command(self, command, arguments, username):
        """
        Handles the built-in command 'enable'.
        The built-in command will be enabled.
        """
        if len(arguments) != 1:
            self.send_chat_message(self.settings["built_in_commands"][command]["help"])
            return

        command_enabled = False
        command_to_enable = arguments[0]

        for built_in_command in self.settings["built_in_commands"]:
            if command_to_enable in self.settings["built_in_commands"][built_in_command]["commands"]:
                self.settings["built_in_commands"][built_in_command]["enabled"] = True
                command_enabled = True
                break

        if not command_enabled:
            for custom_command in self.settings["custom_commands"]:
                if command_to_enable in self.settings["custom_commands"][custom_command]["commands"]:
                    self.settings["custom_commands"][custom_command]["enabled"] = True
                    command_enabled = True
                    break

        if command_enabled:
            self.send_chat_message("Command enabled.")
        else:
            self.send_chat_message(self.settings["built_in_commands"][command]["help"])

        return

    def __handle_disable_command(self, command, arguments, username):
        """
        Handles the built-in command 'disable'.
        The built-in command will be disabled.
        """
        if len(arguments) != 1:
            self.send_chat_message(self.settings["built_in_commands"][command]["help"])
            return

        command_disabled = False
        command_to_disable = arguments[0]

        for built_in_command in self.settings["built_in_commands"]:
            if command_to_disable in self.settings["built_in_commands"][built_in_command]["commands"]:
                self.settings["built_in_commands"][built_in_command]["enabled"] = False
                command_disabled = True
                break

        if not command_disabled:
            for custom_command in self.settings["custom_commands"]:
                if command_to_disable in self.settings["custom_commands"][custom_command]["commands"]:
                    self.settings["custom_commands"][custom_command]["enabled"] = False
                    command_disabled = True
                    break

        if command_disabled:
            self.send_chat_message("Command disabled.")
        else:
            self.send_chat_message(self.settings["built_in_commands"][command]["help"])

        return

    def __handle_add_command(self, command, arguments, username):
        """
        Handles the built-in command 'add'.
        Adds a command to 'custom_commands'.
        """
        if len(arguments) < 2:
            self.send_chat_message(self.settings["built_in_commands"][command]["help"])
            return

        new_command = arguments[0]
        response = " ".join(arguments[1:])

        if new_command not in self.settings["custom_commands"]:
            self.settings["custom_commands"][new_command] = {
                "commands": [
                    new_command
                ],
                "enabled": True,
                "response": response
            }
            self.send_chat_message("Command successfully added!")
        else:
            self.send_chat_message("Command already exists!")
        return

    def __handle_remove_command(self, command, arguments, username):
        """
        Handles the built-in command 'remove'.
        Removes a command from 'custom_commands'.
        """
        if len(arguments) != 1:
            self.send_chat_message(self.settings["built_in_commands"][command]["help"])
            return

        command_to_remove = arguments[0]
        if command_to_remove[0] == "!":
            command_to_remove = command_to_remove[1:]

        if command_to_remove in self.settings["custom_commands"]:
            del self.settings["custom_commands"][command_to_remove]
            self.send_chat_message("Command successfully removed!")
        else:
            self.send_chat_message("Command does not exist!")
        return

    def __handle_edit_command(self, command, arguments, username):
        """
        Handles the built-in command 'edit'.
        Edits the response of the custom command.
        """
        if len(arguments) < 2:
            self.send_chat_message(self.settings["built_in_commands"][command]["help"])
            return

        command_to_edit = arguments[0]
        if command_to_edit[0] == "!":
            command_to_edit = command_to_edit[1:]

        if command_to_edit in self.settings["custom_commands"]:
            edited_response = " ".join(arguments[1:])
            self.settings["custom_commands"][command_to_edit]["response"] = edited_response
            self.send_chat_message("The command was edited successfully!")
        else:
            self.send_chat_message("Command does not exist!")
        return

    def __handle_add_alternate_command(self, command, arguments, username):
        """
        Handles the built-in command 'add_alternate'.
        Adds an additional way to call the same command.
        """
        if len(arguments) != 2:
            self.send_chat_message(self.settings["built_in_commands"][command]["help"])
            return

        command_to_add_to = arguments[0]
        if command_to_add_to in self.settings["custom_commands"]:
            alternate_command = arguments[1]
            self.settings["custom_commands"][command_to_add_to]["commands"].append(alternate_command)
            self.send_chat_message("Alternate command added successfully!")
        else:
            self.send_chat_message("Command does not exist!")
        return

    def __handle_bot_uptime_command(self, command, arguments, username):
        """
        Handles the built-in command 'bot_uptime'.
        Displays what time the bot was started.
        """
        if len(arguments) != 0:
            self.send_chat_message(self.settings["built_in_commands"][command]["help"])
            return

        self.send_chat_message("Bot up since %s" % (self.bot_start_time))
        return

    def __handle_add_quote_command(self, command, arguments, username):
        """
        Handles the built-in command 'add_quote'.
        Adds a user defined quote to the list of quotes.
        """
        if len(arguments) < 1:
            self.send_chat_message(self.settings["built_in_commands"][command]["help"])
            return

        quote = " ".join(arguments)
        new_quote_number = str(len(self.settings["quotes"]) + 1)
        self.settings["quotes"][new_quote_number] = quote
        self.send_chat_message("Quote successfully added.")

        return

    def __handle_get_quote_command(self, command, arguments, username):
        """
        Handles the built-in command 'get_quote'.
        If no quote number is specified, display a random quote, otherwise
        display the quote asked for.
        """
        if len(arguments) > 1:
            self.send_chat_message(self.settings["built_in_commands"][command]["help"])
            return

        if len(arguments) == 1:
            quote_number = arguments[0]
        else:
            number_of_quotes = len(self.settings["quotes"])
            quote_number = str(random.randint(1, number_of_quotes))

        quote = self.settings["quotes"].get(quote_number, None)
        if quote is None:
            number_of_quotes = str(len(self.settings["quotes"]))
            self.send_chat_message("Number of quotes: %s" % (number_of_quotes))
            return

        self.send_chat_message(quote)
        return

    def __add_points_to_everyone(self, points_for_everyone):
        """
        Adds specified number of points to everyone that currently has any
        number of points.
        """
        for user in self.settings["points"]:
            self.settings["points"][user] += points_for_everyone
        if points_for_everyone > 1:
            string_to_use = "%d %ss added for everyone!" % (points_for_everyone, self.settings["points_system"]["name"])
        else:
            string_to_use = "%d %s added for everyone!" % (points_for_everyone, self.settings["points_system"]["name"])
        self.send_chat_message(string_to_use)
        return

    def __add_points_to_user(self, user, points_for_user):
        """
        Adds number of points to a specific user.
        If the user does not exist in the list, then add them and give them
        points.
        """
        user_lower = user.lower()
        if user_lower not in self.settings["points"]:
            self.settings["points"][user_lower] = 0
        self.settings["points"][user_lower] += points_for_user
        if points_for_user == 0 or points_for_user > 1:
            string_to_use = "%d %ss added to %s!" % (points_for_user, self.settings["points_system"]["name"], user)
        else:
            string_to_use = "%d %s added to %s!" % (points_for_user, self.settings["points_system"]["name"], user)
        self.send_chat_message(string_to_use)
        return

    def __handle_add_points_command(self, command, arguments, username):
        """
        Handles the add points command.
        """
        if len(arguments) > 2:
            self.send_chat_message(self.settings["built_in_commands"][command]["help"])
            return
        # The intent would be to add points to everyone.
        # TODO(Chris): This would require getting a list of users currently in
        # the room, then adding the points to each of them. An alternate way of
        # handling this would be to always add the person to the points list
        # the first time they join the room.
        if len(arguments) == 1:
            try:
                points_for_everyone = int(arguments[0])
                self.__add_points_to_everyone(self, points_for_everyone)
            except:
                self.send_chat_message(self.settings["built_in_commands"][command]["help"])
        # The intent is to only add points to one user.
        if len(arguments) == 2:
            try:
                points_for_user = int(arguments[1])
                self.__add_points_to_user(arguments[0], points_for_user)
            except:
                self.send_chat_message(self.settings["built_in_commands"][command]["help"])
        return

    def __get_points_from_user(self, user):
        """
        Gets the number of points from the specified user.
        """
        user_lower = user.lower()
        if user_lower not in self.settings["points"]:
            self.settings["points"][user_lower] = 0
        user_points = self.settings["points"][user_lower]
        if user_points == 0 or user_points > 1:
            string_to_use = "%s has %d %ss!" % (user, user_points, self.settings["points_system"]["name"])
        else:
            string_to_use = "%s has %d %s!" % (user, user_points, self.settings["points_system"]["name"])
        self.send_chat_message(string_to_use)
        return

    def __handle_get_points_command(self, command, arguments, badges, username):
        """
        Handles the get points command.
        """
        if len(arguments) > 1:
            self.send_chat_message(self.settings["built_in_commands"][command]["help"])
            return
        if len(arguments) == 1 and (BADGE_MOD in badges or BADGE_BROADCASTER in badges):
            username = arguments[0]
        self.__get_points_from_user(username)
        return

    def __handle_built_in_command(self, command, arguments, badges, username):
        """
        Handles built-in commands.
        """
        # Commands that "normal" people should not be able to use.
        if BADGE_BROADCASTER in badges or BADGE_MOD in badges:
            if command == "enable_command":
                self.__handle_enable_command(command, arguments, username)
            elif command == "disable_command":
                self.__handle_disable_command(command, arguments, username)
            elif command == "add_command":
                self.__handle_add_command(command, arguments, username)
            elif command == "remove_command":
                self.__handle_remove_command(command, arguments, username)
            elif command == "edit_command":
                self.__handle_edit_command(command, arguments, username)
            elif command == "add_alternate_command":
                self.__handle_add_alternate_command(command, arguments, username)
            elif command == "bot_uptime_command":
                self.__handle_bot_uptime_command(command, arguments, username)
            elif command == "add_points_command":
                self.__handle_add_points_command(command, arguments, username)

        # Commands that everyone will be able to use.
        if command == "help_command":
            self.__handle_help_command(command, arguments, username)
        elif command == "add_quote_command":
            self.__handle_add_quote_command(command, arguments, username)
        elif command == "get_quote_command":
            self.__handle_get_quote_command(command, arguments, username)
        elif command == "get_points_command":
            self.__handle_get_points_command(command, arguments, badges, username)

        return

    def _run_built_in_command(self, command, arguments, badges, username):
        """
        Runs the command from the built-in command list.
        """
        for built_in_command in self.settings["built_in_commands"]:
            command_enabled = self.settings["built_in_commands"][built_in_command]["enabled"]
            valid_command = command in self.settings["built_in_commands"][built_in_command]["commands"]
            if command_enabled and valid_command:
                self.__handle_built_in_command(built_in_command, arguments, badges, username)
                break
        return

    def _check_built_in_commands(self, command):
        """
        Checks if the command is a built-in command.
        """
        command_exists = False
        for built_in_command in self.settings["built_in_commands"]:
            command_enabled = self.settings["built_in_commands"][built_in_command]["enabled"]
            valid_command = command in self.settings["built_in_commands"][built_in_command]["commands"]
            if command_enabled and valid_command:
                command_exists = True
                break
        return command_exists

    def _run_custom_command(self, command, arguments, badges, username):
        """
        Runs the command from the custom command list.
        """
        for custom_command in self.settings["custom_commands"]:
            command_enabled = self.settings["custom_commands"][custom_command]["enabled"]
            valid_command = command in self.settings["custom_commands"][custom_command]["commands"]
            if command_enabled and valid_command:
                self.send_chat_message(self.settings["custom_commands"][custom_command]["response"])
                break
        return

    def _check_custom_commands(self, command):
        """
        Checks if the command is the custom command list.
        """
        command_exists = False
        for custom_command in self.settings["custom_commands"]:
            command_enabled = self.settings["custom_commands"][custom_command]["enabled"]
            valid_command = command in self.settings["custom_commands"][custom_command]["commands"]
            if command_enabled and valid_command:
                command_exists = True
                break
        return command_exists

    def check_for_commands(self, message, badges, username):
        """
        Checks if the message contains a command.
        """
        if not self.allow_bang_commands:
            return

        message_words = message.split(" ")
        if (len(message_words) > 0) and (len(message_words[0]) > 0) and (message_words[0][0] == "!"):
            command = message_words[0][1:]
            if len(message_words) > 1:
                arguments = message_words[1:]
            else:
                arguments = []
            if self._check_built_in_commands(command):
                self._run_built_in_command(command, arguments, badges, username)
            elif self._check_custom_commands(command):
                self._run_custom_command(command, arguments, badges, username)
        return

    def _handle_buffer(self, buff):
        """
        Handles messages from the incoming buffer.
        """
        for line in buff:
            self.settings["current_messages_received"] += 1
            self.settings["current_messages"] += 1
            self.printlog(line, DEBUG)
            # TODO(CHris): This will need to be improved on...
            if line.find("PING") == 0:
                parts = line.split(" ")
                pong_str = parts[1]
                self.printlog("Sending PONG", DEBUG)
                self.__send_message("PONG %s" % (pong_str))
            elif line[0] == ":":
                parts = line[1:].split(" ")
                # TODO(Chris): Get the display-name. The name used is the all lower-case version of the user.
                username = parts[0].split("!")[0]
                if parts[1] == "JOIN":
                    self.printlog("%s joined the channel." % (username), INFO)
                elif parts[1] == "PART":
                    self.printlog("%s left the channel." % (username), INFO)
            elif line[0] == "@":
                self.settings["current_messages_between_timed_commands"] += 1
                info, message = self.get_info_and_message(line)

                if info["message_type"] == "PRIVMSG":
                    if "\x01ACTION" in message:
                        message = message.replace("\x01", "")
                        message = message.replace("ACTION", "")
                    username = info["connection_info"].split("!")[0]
                    if len(info["display_name"]) > 0:
                        username = info["display_name"]
                    badges = self.get_badges(info)
                    self.printlog("%s: %s" % (badges + username, message))

                    # see if the message contains any commands
                    self.check_for_commands(message, badges, username)
                else:
                    self.printlog(line, WARN)
            else:
                self.printlog(line, WARN)
        return

    def _handle_timed_commands(self):
        """
        Handles commands that are activated after a set amount of time.
        """
        if not self.settings["run_timed_commands"]:
            return

        current_messages = self.settings["current_messages_between_timed_commands"]
        minimum_messages = self.settings["minimum_messages_between_timed_commands"]
        if current_messages < minimum_messages:
            return

        self.settings["current_messages_between_timed_commands"] = 0
        current_time = datetime.datetime.now()
        if self.settings["randomly_select_timed_command"]:
            random_number = random.randint(0, len(self.settings["timed_commands"]) - 1)
            key = list(self.settings["timed_commands"].keys())[random_number]
            time_delta = (current_time - self.settings["timed_commands"][key]["start_time"]).total_seconds()
            if time_delta > self.settings["timed_commands"][key]["interval"]:
                self.send_chat_message(self.settings["timed_commands"][key]["message"])
                log_string = None
                if self.display_name is not None:
                    log_string = "%s: %s" % (self.display_name, self.settings["timed_commands"][key]["message"])
                else:
                    log_string = "%s: %s" % (self.nick, self.settings["timed_commands"][key]["message"])
                self.printlog(log_string)
                for command in self.settings["timed_commands"]:
                    self.settings["timed_commands"][command]["start_time"] = current_time
        else:
            for command in self.settings["timed_commands"]:
                time_delta = (current_time - self.settings["timed_commands"][command]["start_time"]).total_seconds()
                if time_delta > self.settings["timed_commands"][command]["interval"]:
                    self.send_chat_message(self.settings["timed_commands"][command]["message"])
                    log_string = None
                    if self.display_name is not None:
                        log_string = "%s: %s" % (self.display_name, self.settings["timed_commands"][command]["message"])
                    else:
                        log_string = "%s: %s" % (self.nick, self.settings["timed_commands"][command]["message"])
                    self.printlog(log_string)
                    self.settings["timed_commands"][command]["start_time"] = current_time
        return

    def run(self):
        """
        The main loop of control for the bot.
        """
        if self.server_connection is None:
            return "server connection not established"
        try:
            for command in self.settings["timed_commands"]:
                self.settings["timed_commands"][command]["start_time"] = datetime.datetime.now()
            while self.keep_running:
                try:
                    temp = self.server_connection.recv(1024)
                    self.readbuffer = self.readbuffer + temp.decode("UTF-8")
                    temp = self.readbuffer.split("\r\n")
                    self.readbuffer = temp.pop()

                    self._handle_buffer(temp)
                    self._handle_timed_commands()
                except Exception as exception:
                    err = exception.args[0]
                    if err == errno.EAGAIN or err == errno.EWOULDBLOCK:
                        time.sleep(0.1)
        except KeyboardInterrupt as keyboard_interrupt:
            self.printlog(sys.exc_info()[0], WARN)
            self.printlog(traceback.format_exc(), WARN)
            self.printlog(str(keyboard_interrupt), WARN)
        except InterruptedError as interrupted_error:
            self.printlog(sys.exc_info()[0], WARN)
            self.printlog(traceback.format_exc(), WARN)
            self.printlog(str(interrupted_error), WARN)
        except Exception as exception:
            self.printlog(sys.exc_info()[0], WARN)
            self.printlog(traceback.format_exc(), WARN)
            self.printlog(str(exception), WARN)
        finally:
            self.close()

        return

    def close(self):
        """
        Closes the connection to the chat.
        """
        try:
            if self.server_connection is not None:
                self.server_connection.close()
                self.server_connection = None
        except Exception as exception:
            self.printlog("Exception trying to close the socket.", WARN)
            self.printlog(str(exception), WARN)

        self._save_settings()

        return

    def __save_settings_file(self, file_name, data, sort_key_num=False):
        """
        Saves the state of the bot.
        """
        successful = True
        sort_keys = True
        if sort_key_num:
            sort_keys = False
            ordered_keys = sorted([int(x) for x in data.keys()])
            ordered_dict = collections.OrderedDict()
            for key in ordered_keys:
                key = str(key)
                ordered_dict[key] = data[key]
            data = ordered_dict

        if self.settings is not None:
            try:
                settings_file_path = self.__get_file_path(file_name)
                settings_file = open(settings_file_path, "w")
                try:
                    json.dump(data, settings_file, sort_keys=sort_keys, indent=4)
                    settings_file.write("\n")
                except Exception as exception:
                    successful = False
                    self.printlog("Error trying to write to file: %s" % (settings_file_path), WARN)
                    self.printlog(str(exception), WARN)
                finally:
                    settings_file.close()
            except Exception as exception:
                successful = False
                self.printlog("Error trying to open file: %s" % (settings_file_path), WARN)
                self.printlog(str(exception), WARN)
        else:
            successful = False
            self.printlog("No settings to save.", WARN)

        if not successful:
            self.printlog(json.dumps(data, sort_keys=True, indent=4), WARN)

        return successful

    def _save_settings(self):
        """
        Saves all of the settings files to be used for later.
        """
        # Make sure certain variables are updated.
        self.settings["total_messages"] += self.settings["current_messages"]
        self.settings["total_messages_received"] += self.settings["current_messages_received"]
        self.settings["total_messages_sent"] += self.settings["current_messages_sent"]

        # Make sure certain variables are cleared/reset before saving.
        for command in self.settings["timed_commands"]:
            self.settings["timed_commands"][command]["start_time"] = None
        self.settings["current_messages"] = 0
        self.settings["current_messages_received"] = 0
        self.settings["current_messages_sent"] = 0
        self.settings["current_messages_between_timed_commands"] = 0

        successful = {}

        successful[TwitchBot.POINTS_FILENAME] = self.__save_settings_file(TwitchBot.POINTS_FILENAME, self.settings["points"])
        successful[TwitchBot.CHAT_PACKS_FILENAME] = self.__save_settings_file(TwitchBot.CHAT_PACKS_FILENAME, self.settings["chat_packs"])
        successful[TwitchBot.TIMED_COMMANDS_FILENAME] = self.__save_settings_file(TwitchBot.TIMED_COMMANDS_FILENAME, self.settings["timed_commands"])
        successful[TwitchBot.QUOTES_FILENAME] = self.__save_settings_file(TwitchBot.QUOTES_FILENAME, self.settings["quotes"], sort_key_num=True)
        successful[TwitchBot.CUSTOM_COMMANDS_FILENAME] = self.__save_settings_file(TwitchBot.CUSTOM_COMMANDS_FILENAME, self.settings["custom_commands"])
        successful[TwitchBot.BUILT_IN_COMMANDS_FILENAME] = self.__save_settings_file(TwitchBot.BUILT_IN_COMMANDS_FILENAME, self.settings["built_in_commands"])

        points_temp = self.settings["points"]
        chat_packs_temp = self.settings["chat_packs"]
        timed_commands_temp = self.settings["timed_commands"]
        quotes_temp = self.settings["quotes"]
        custom_commands_temp = self.settings["custom_commands"]
        built_in_commands_temp = self.settings["built_in_commands"]
        self.settings["points"] = None
        self.settings["chat_packs"] = None
        self.settings["timed_commands"] = None
        self.settings["quotes"] = None
        self.settings["custom_commands"] = None
        self.settings["built_in_commands"] = None
        successful[TwitchBot.SETTINGS_FILENAME] = self.__save_settings_file(TwitchBot.SETTINGS_FILENAME, self.settings)
        self.settings["points"] = points_temp
        self.settings["chat_packs"] = chat_packs_temp
        self.settings["timed_commands"] = timed_commands_temp
        self.settings["quotes"] = quotes_temp
        self.settings["custom_commands"] = custom_commands_temp
        self.settings["built_in_commands"] = built_in_commands_temp

        failed = False
        for setting in successful:
            if not successful[setting]:
                self.printlog("%s saved unsuccessfully." % (setting), WARN)
                failed = True

        if failed:
            self.printlog(json.dumps(self.settings, sort_keys=True, indent=4), WARN)

        return successful

    def __read_settings_file(self, file_name):
        """
        Reads and sets the state of the bot.
        """
        settings = None
        settings_file_path = self.__get_file_path(file_name)

        if os.path.isfile(settings_file_path):
            try:
                settings_file = open(settings_file_path, "r")
                try:
                    settings = json.load(settings_file)
                except Exception as exception:
                    self.printlog("Error trying to read from file: %s" % (settings_file_path), WARN)
                    self.printlog(sys.exc_info()[0], WARN)
                    self.printlog(traceback.format_exc(), WARN)
                    self.printlog(str(exception), WARN)
                finally:
                    settings_file.close()
            except Exception as exception:
                self.printlog("Error trying to open file: %s" % (settings_file_path), WARN)
                self.printlog(str(exception), WARN)
        else:
            self.printlog("File not found: %s." % (settings_file_path), WARN)

        return settings

    def _get_settings(self):
        """
        Get all settings that determine how the bot will function.
        """
        settings = self.__read_settings_file(TwitchBot.SETTINGS_FILENAME)
        if settings is not None:
            settings["built_in_commands"] = self.__read_settings_file(TwitchBot.BUILT_IN_COMMANDS_FILENAME)
            settings["custom_commands"] = self.__read_settings_file(TwitchBot.CUSTOM_COMMANDS_FILENAME)
            settings["quotes"] = self.__read_settings_file(TwitchBot.QUOTES_FILENAME)
            settings["timed_commands"] = self.__read_settings_file(TwitchBot.TIMED_COMMANDS_FILENAME)
            settings["chat_packs"] = self.__read_settings_file(TwitchBot.CHAT_PACKS_FILENAME)
            settings["points"] = self.__read_settings_file(TwitchBot.POINTS_FILENAME)

        return settings

    def printlog(self, to_print, debug_level=None, hide_text=None):
        """
        Handles both printing to the console and logging to a file.
        """
        current_time = datetime.datetime.now()
        print_time = current_time.strftime("%H:%M:%S")
        log_time = current_time.strftime("%Y-%m-%d %H:%M:%S.%f")

        corrected_str = self.__hide_text(to_print, hide_text)
        print_str = "%s %s" % (print_time, corrected_str)
        log_str = "%s %s" % (log_time, corrected_str)

        if debug_level == DEBUG:
            logging.debug(log_str)
        elif debug_level == INFO:
            logging.info(log_str)
        elif debug_level == WARN:
            logging.warning(log_str)
        elif debug_level == ERROR:
            logging.error(log_str)
        else:
            logging.log(INFO, log_str)

        if debug_level is None:
            print(self.wrapper.fill(print_str))
        elif self.settings["verbose_debugging"]:
            prefix = ""
            if debug_level == DEBUG:
                prefix = "DEBUG"
            elif debug_level == INFO:
                prefix = "INFO"
            elif debug_level == WARN:
                prefix = "WARN"
            elif debug_level == ERROR:
                prefix = "ERROR"
            print_str = "%-6s%s" % (prefix, print_str)
            print(self.wrapper.fill(print_str))

        return

if __name__ == "__main__":
    BOT = TwitchBot()
    BOT.connect()
    BOT.run()
    BOT.close()
