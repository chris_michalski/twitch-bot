# README #

### What is this repository for? ###

* This is a bot for Twitch.tv chat. The intention is for it to be a streamer's bot that would be used for good.

### Goals of this project? ###

* Be cross-platform.
* No 3rd party modules.

### How do I get set up? ###

* Install Python 3.X
* Run the Python program.

### Contribution guidelines ###

* Writing tests
    * If it makes sense to write a test, write a test.
* Code review
    * Code will have to be reviewed by at least active contributor, but go through an admin to make it offical.
* Other guidelines
    * Comments are good.

### Who do I talk to? ###

* Repo owner or admin
    * Chris Michalski (cammichalski@gmail.com)
* Other community or team contact
    * N/A